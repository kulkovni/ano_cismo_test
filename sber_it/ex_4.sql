/*
Представим, что между источником и хранилищем настроена репликация данных следующего вида:

при любом изменении данных в таблице table источника: вставке, модификации или удалению строки -
происходит вставка одной новой строки со всеми полями источника в таблицу table хранилища.
Старые версии строки в хранилище при этом не затрагиваются - не изменяются и не удаляются.

При этом в таблице table хранилища есть 2 дополнительных служебных поля:
•	ts типа timestamp, куда записывается временная метка изменения
•	operation_cd типа varchar, куда записывается код операции: 'insert', 'update', 'delete'
Известно также, что в таблице источника есть поле id, которое является первичным ключом.

Нужно написать запрос над таблицей хранилища, который выведет текущее наполнение таблицы источника.

*/

-- через left join с таблицей источником,
--    при этом в хранилище нужно отобрать в окне максимальные даты в атрибуте ts

-- table_source = таблица источник
-- tbale_sink = таблица хранилище


/*ddl*/

drop table if exists sber_ex_4.table_source;
create table sber_ex_4.table_source(
    id serial primary key not null unique,
    value_1 varchar(30)
);

drop table if exists sber_ex_4.table_sink;
create table sber_ex_4.table_sink(
    id bigint not null,
    value_1 varchar(30),
    ts timestamp,
    operation_cd varchar(25)
);

insert into sber_ex_4.table_source (value_1) values ('hello'),('world'),('my'),('name'),('is'),('nobody!'),
                                                    ('What is'),('you'),('name?');

update sber_ex_4.table_source set value_1 = 'Misha' WHERE value_1 = 'nobody!';
delete from sber_ex_4.table_source where value_1 = 'hello';

insert into sber_ex_4.table_sink (id, value_1, ts, operation_cd)
values(1, 'hello', '2022-01-01', 'insert'),(2, 'world', '2022-01-01', 'insert'),(3, 'my','2022-01-01', 'insert'),
      (4, 'name', '2022-01-01', 'insert'),(5, 'is', '2022-01-01', 'insert'),(6, 'nobody!', '2022-01-01', 'insert'),
      (7, 'What is', '2022-01-01', 'insert'),(8, 'you', '2022-01-01', 'insert'),(9, 'name?', '2022-01-01', 'insert');


insert into sber_ex_4.table_sink(id, value_1, ts, operation_cd)
values (6, 'Misha', '2022-02-01', 'update'),
       (1, 'hello', '2022-03-01', 'delete');

/*
DML
*/

select * from sber_ex_4.table_source order by id;
select * from sber_ex_4.table_sink;

with cte as (
    select si.id, si.value_1, rank() over (partition by si.id order by si.ts desc) rank_
    from sber_ex_4.table_sink si
    where si.operation_cd != 'delete'
)
select id, value_1 from cte where rank_ = 1 order by id;
