/*
Дана таблица с историей изменений, т.е. по клиенту есть предложения, по которым вносились изменения.
Меняться могут сумма или срок предложения.
Изменения можно отследить по полю дата изменения предложения

Требуется построить историю изменения предложения и отдельным полем показывать
дельту в сумме предложения от предыдущего состояния
*/

/* DDL */

drop table if exists sber_ex_6.bank;
create table sber_ex_6.bank(
    cust_id	varchar(50) not null,
    offer_type varchar(80) not null,
    offer_id varchar(20) not null,
    offer_sum numeric(6),
    offer_dur smallint,
    offer_date date,
    offer_change date
);

insert into sber_ex_6.bank(cust_id,	offer_type,	offer_id,	offer_sum,
                           offer_dur,	offer_date,	offer_change)
values('cust1', 'Инвестиционный Кредит',	'offer_1',	50000,	10,	'2022-01-20',	'2022-01-20'),
('cust2', 'Оборотный Кредит',	'offer_2',	400,	2,	'2022-01-23',	'2022-01-23'),
('cust1', 'Овердрафт',	'offer_3',	2500,	1,	'2022-03-17',	'2022-03-17'),
('cust1', 'Инвестиционный Кредит',	'offer_1',	90000,	10,	'2022-01-20',	'2022-02-01'),
('cust3', 'Целевой Кредит',	'offer_4',	780,	4,	'2022-02-23',	'2022-02-23'),
('cust3', 'Овердрафт',	'offer_5',	120,	3,	'2022-05-04' , '2022-05-04'),
('cust4', 'Инвестиционный Кредит',	'offer_6',	800,	2,	'2022-01-10', '2022-01-10'),
('cust5', 'Лизинг',	'offer_7',	900,	15,	'2022-06-18' ,	'2022-06-18'),
('cust1', 'Инвестиционный Кредит', 'offer_1',	5000,	10,	'2022-01-20', 	'2022-04-12'),
('cust3', 'Целевой Кредит', 'offer_4',	780,	4,	'2022-02-23',	'2022-04-10'),
('cust3', 'Целевой Кредит', 'offer_4',	1000,	4, '2022-02-23',	'2022-06-01');

-- для наглядности добавим еще данных
insert into  sber_ex_6.bank(cust_id,	offer_type,	offer_id,	offer_sum,
                           offer_dur,	offer_date,	offer_change)
values ('cust1', 'Овердрафт',	'offer_3',	1590,	1,	'2022-03-17',	'2022-03-23'),
       ('cust2', 'Оборотный Кредит',	'offer_2',	290,	2,	'2022-01-23',	'2022-03-23'),
       ('cust3', 'Овердрафт',	'offer_5',	290,	3,	'2022-05-04',	'2022-08-10'),
       ('cust4', 'Инвестиционный Кредит',	'offer_6',	1400,	2,	'2022-01-10',	'2022-02-10'),
       ('cust5', 'Лизинг',	'offer_7',	3000,	15,	'2022-06-18',	'2022-09-10');

/*
DML
*/

-- Требуется построить историю изменения предложения и отдельным полем показывать
-- дельту в сумме предложения от предыдущего состояния
select cust_id, offer_type, offer_id,
       offer_sum, offer_dur, offer_date, offer_change,
       (offer_sum - lag(offer_sum) over (partition by offer_id order by cust_id, offer_change)) as delta
from sber_ex_6.bank
order by cust_id, offer_id, offer_change;