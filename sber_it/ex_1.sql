/* DDL */
DROP TABLE IF EXISTS sber_ex_1.department;
CREATE TABLE sber_ex_1.department (
    id serial NOT NULL unique ,
    name VARCHAR(100) NOT NULL
);

insert into sber_ex_1.department(name) values('Finansists'), ('IT'), ('HR'), ('Lawyers');

DROP TABLE IF EXISTS sber_ex_1.employee;
CREATE TABLE sber_ex_1.employee (
    id serial NOT NULL unique,
    department_id bigint NOT NULL,
    chief_id bigint null,
    name varchar(100),
    salary numeric (6),
    FOREIGN KEY (department_id)  REFERENCES sber_ex_1.department (id) ON DELETE CASCADE,
    FOREIGN KEY (chief_id) REFERENCES  sber_ex_1.employee(id) ON DELETE CASCADE
);

Insert into sber_ex_1.employee(department_id, chief_id, name, salary)
Values (1, NULL, 'Ben', 60000),
       (1, NULL, 'Jhon', 75000),
       (1, 1, 'Nicolas', 40000),
       (1, 1, 'Styart', 80000),
       (1, 1, 'Many', 40000),
       (2, NULL, 'Man', 50000),
       (2, 2, 'Alfrad', 50000),
       (3, NULL, 'Alfrad2', 45000),
       (3, 3, 'Monika', 46000),
       (3, 3, 'Alf',34000),
       (4,NULL, 'Alexandr', 56000),
       (4, 4, 'Nik', 58000);


/* DML */

-- Вывести список сотрудников, получающих заработную плату большую чем у непосредственного руководителя:
select first.id, first.name
from sber_ex_1.employee first, sber_ex_1.employee sec
where sec.id = first.chief_id and first.salary > sec.salary;


-- Вывести список сотрудников, получающих максимальную заработную плату в своем отделе
select e.id employ_id, e.name
from sber_ex_1.employee e
where e.salary = (select max(salary)
                  from sber_ex_1.employee e2 where e.department_id = e2.department_id);


-- Вывести список ID отделов, количество сотрудников в которых не превышает 3 человек:
select d.id, d.name
from sber_ex_1.department d
inner join sber_ex_1.employee e on e.department_id = d.id
group by d.id, d.name
having count(d.id) <= 3;

-- Вывести список сотрудников, не имеющих назначенного руководителя, работающего в том же отделе

/*
С этой задачей не справился, т.к. не совсем понимаю постановку и представляется,
что не хватает явного атрибута с
флагом руководитлеь / не руководитель .

обазначу такого сотруника по инсертам  (такой сотрудник всего 1)

    `(1, NULL, 'Jhon', 75000),`

но исходя из состава атрибутов - нельзя определить является ли он руководителем или нет

ниже приведен запрос по условию задачи (вне комментария), но его можно описать и проще:

    `select * from sber_ex_1.employee e where e.chief_id is null;`
*/

select distinct e1.id, e1.name,  e1.department_id, e1.chief_id from sber_ex_1.employee e1
left join sber_ex_1.employee e2 on e1.department_id = e2.department_id
where e1.chief_id is null;


-- Найти список ID отделов с максимальной суммарной зарплатой сотрудников
select d.id, d.name, d.summary from (
    select d1.id, d1.name, sum(e.salary) summary
    from sber_ex_1.department d1
    inner join sber_ex_1.employee e on d1.id = e.department_id
    group by d1.id, d1.name) d
order by d.summary desc limit 1;