-- Есть таблица без истории изменений, т.е. по клиенту может быть несколько
-- предложений,и это все разные предложения, а не одни и
-- те же с новыми параметрами

/* DDL */

DROP TABLE IF EXISTS sber_ex_5.bank;
create table sber_ex_5.bank (
    cust_id varchar(40),
    offer_type varchar(50),
    offer_sum numeric(10) not null,
    offer_dur int8 not null,
    offer_date date
);

insert into sber_ex_5.bank values ('cust1', 'Инвестиционный Кредит', 5000000, 10, '2022.01.20'),
                                  ('cust2',	'Оборотный Кредит',	350000,	2, '2022-01-23'),
                                  ('cust1', 'Овердрафт',	250000,	1,	'2022-03-17'),
                                  ('cust3', 'Целевой Кредит',	780000,	4,	'2022-02-23'),
                                  ('cust3', 'Овердрафт',	220000,	3,	'2022-05-04'),
                                  ('cust4', 'Инвестиционный Кредит',	100000,	1,	'2022-01-10'),
                                  ('cust1', 'Лизинг',	300000,	15,	'2022-06-18'),
                                  ('cust2', 'Инвестиционный Кредит',	200000,	20, '2022-09-05');


/* DML */
-- 1. Количество клиентов, у которых сумма предложений больше или равно 400 млн.

-- так по существу эта задача мало отличиается от следующей - предположу, что требуется группировка
-- по какому то конкретному типу предложений. Реализовал через where в конце запроса

select count(distinct(cust_id))
from (
    select cust_id, offer_type, sum(offer_sum)
    from sber_ex_5.bank
    group by cust_id, offer_type
    having sum(offer_sum) >= 400000000) bank_table
where offer_type = 'Целевой Кредит';


--2. Количество клиентов, у которых сумма по всем типам предложений больше 1 млрд
select count(distinct(cust_id))
from (
    select cust_id, sum(offer_sum)
    from sber_ex_5.bank
    group by cust_id
    having sum(offer_sum) >= 1000000000) bank_table;


-- 3. Доля (в %) предложений по кредиту со сроком более 2 лет (не включительно) в разрезе типов
select offer_type,
    round(sum(offer_sum) / (
    select sum(offer_sum) from sber_ex_5.bank) , 4) * 100 percent
from sber_ex_5.bank
where offer_type != 'Лизинг' and offer_dur < 2
group by offer_type;


-- 4 Добавить в п.3 флаг для тех, у кого доля больше 60%
drop view if exists sber_ex_5.procents;

create view sber_ex_5.procents as
    select offer_type,
           round(sum(offer_sum) / (select sum(offer_sum) from sber_ex_5.bank) , 4) * 100 percent
    from sber_ex_5.bank
    where offer_type != 'Лизинг' and offer_dur < 2
    group by offer_type;
--
select offer_type, percent,
    case
        when percent > 60.0 then 'true'
        when percent < 60.0 then 'false'
    end flag
from sber_ex_5.procents;
drop view if exists sber_ex_5.procents;
