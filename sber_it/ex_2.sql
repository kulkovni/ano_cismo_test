-- Дана таблица student_marks с 2 столбцами: name и mark. Соответственно, ФИО ученика и оценка.
-- Считаем, что среди учеников нет двоих с одинаковым ФИО. У одного ученика может быть неограниченное количество любых оценок.
-- Нужно написать запрос, который выведет ФИО учеников и количество пятерок для тех, у кого количество двоек равно 10 или меньше.
-- Написать решение без использования подзапросов.


/* DDL */
drop table if exists sber_ex_2.student_marks;
create table sber_ex_2.student_marks(
    name varchar(100),
    mark int
);

Insert into sber_ex_2.student_marks(name,mark)
Values ('pupkin vasya dmitrievich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('markow dima mihaylovich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('pupkin vasya dmitrievich', 3);
Insert into sber_ex_2.student_marks(name,mark)
Values ('pupkin vasya dmitrievich', 5);
Insert into sber_ex_2.student_marks(name,mark)
Values ('pupkin vasya dmitrievich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('pupkin vasya dmitrievich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('pupkin vasya dmitrievich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('pupkin vasya dmitrievich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('pupkin vasya dmitrievich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('pupkin vasya dmitrievich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('pupkin vasya dmitrievich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('pupkin vasya dmitrievich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('pupkin vasya dmitrievich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('pupkin vasya dmitrievich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('pupkin vasya dmitrievich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('pupkin vasya dmitrievich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('pupkin vasya dmitrievich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('pupkin vasya dmitrievich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('markow dima mihaylovich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('markow dima mihaylovich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('markow dima mihaylovich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('markow dima mihaylovich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('markow dima mihaylovich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('markow dima mihaylovich', 5);
Insert into sber_ex_2.student_marks(name,mark)
Values ('markow dima mihaylovich', 3);
Insert into sber_ex_2.student_marks(name,mark)
Values ('markow dima mihaylovich', 5);
Insert into sber_ex_2.student_marks(name,mark)
Values ('mihailov ivan dmitrievich', 5);
Insert into sber_ex_2.student_marks(name,mark)
Values ('mihailov ivan dmitrievich', 5);
Insert into sber_ex_2.student_marks(name,mark)
Values ('mihailov ivan dmitrievich', 5);
Insert into sber_ex_2.student_marks(name,mark)
Values ('mihailov ivan dmitrievich', 5);
Insert into sber_ex_2.student_marks(name,mark)
Values ('mihailov ivan dmitrievich', 5);
Insert into sber_ex_2.student_marks(name,mark)
Values ('mihailov ivan dmitrievich', 5);
Insert into sber_ex_2.student_marks(name,mark)
Values ('mihailov ivan dmitrievich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('mihailov ivan dmitrievich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('mihailov ivan dmitrievich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('mihailov ivan dmitrievich', 2);
Insert into sber_ex_2.student_marks(name,mark)
Values ('mihailov ivan dmitrievich', 2);

/* DML */

select name, sum(case when mark = 5 then 1 else 0 end) as cnt
from sber_ex_2.student_marks
group by name
having sum(case when mark = 2 then 1 else 0 end) <= 10;