/* DML */
-- а) Кто тратит больше - мужчины или женщины?
-- через окно
select distinct
    case
        when cli.gender_id = 1 then 'Сумма трат женщин'
        when cli.gender_id = 2 then 'Сумма трат мужчин' end,
                sum(tr.trx_amt) over (partition by cli.gender_id)
    from sber_ml_replications.clients_ref cli
        left join sber_ml_replications.cards_ref cr using (client_id)
        left join sber_ml_replications.trx_ref tr on cr.card_id = tr.card_id
    where tr.card_id is not null and cli.gender_id is not null;

-- через группировку
select
    case
        when cli.gender_id = 1 then 'Сумма трат женщин'
        when cli.gender_id = 2 then 'Сумма трат мужчин' end,
    sum(tr.trx_amt)
    from sber_ml_replications.clients_ref cli
        left join sber_ml_replications.cards_ref cr using (client_id)
        left join sber_ml_replications.trx_ref tr on cr.card_id = tr.card_id
    where tr.card_id is not null
      and cli.gender_id is not null and tr.status_id = 1
    group by cli.gender_id;

-- А кто чаще?
select distinct
    case
        when cli.gender_id = 1 then 'Кол-во транзакций женщин'
        when cli.gender_id = 2 then 'Кол-во транзакций мужчин' end,
    count(tr.trx_amt) over (partition by cli.gender_id)
    from sber_ml_replications.clients_ref cli
        left join sber_ml_replications.cards_ref cr using (client_id)
        left join sber_ml_replications.trx_ref tr on cr.card_id = tr.card_id
    where tr.card_id is not null and cli.gender_id is not null;

------------------------------------------------------------------------------------------------------------

--б) Выведи транзакций клиента(ов) с самыми частыми тратами,
-- добавь столбик с накопительным итогом по дням.
-- находим кол-во транзакций по каждой карте:
with cte_trx as (
   select trx.card_id,
       rank() over (partition by trx.card_id order by trx.trx_amt) rank_
   from sber_ml_replications.trx_ref trx
   where trx.card_id is not null and trx.trx_amt != 0
)
select card_id, max(rank_) cnt_transactions
from cte_trx
group by card_id
order by max(rank_) desc;

/*
Топ 3 по кол-ву операций:
card_id, cntr_trx
114, 18
21, 18
51, 18
*/
-- на интерес - смотрим чьи это карты
select  from sber_ml_replications.cards_ref rr
left join sber_ml_replications.clients_ref using(client_id)
where card_id in (114, 21, 51);

/*
топ 3 "транжир":
client_id, card_id, card_name, open_dt, close_dt
3,114,MasterCard Classic,2020-04-12,,2,36,true
38,21,Visa Infinite,2020-07-31,,2,36,true
41,51,Visa Classic,2020-11-14,,2,47,true
*/

-- зная ИДшники карт - ищем нужные транзакции с итоговым условием задачи (НИ + персданные карт и ИД клиента):
-- но сначала дадим вьюшку, что бы итоговый запрос проще читался. В таком представлении  будет хранится:
-- клиент, карта, кол-во транзакций по периоду:

create view sber_ml_replications.max_transaction as (
with cte_trx as (
   select cli.client_id, trx.card_id,
       rank() over (partition by trx.card_id order by trx.trx_amt) rank_
   from sber_ml_replications.trx_ref trx
   inner join sber_ml_replications.cards_ref crd using (card_id)
   inner join sber_ml_replications.clients_ref cli on crd.client_id = cli.client_id
   where trx.card_id is not null and trx.trx_amt != 0
)
select client_id, card_id, max(rank_) cnt_transactions
from cte_trx
group by client_id, card_id
order by max(rank_) desc);

-- вьюшка обозначит топ трат, но для вывода накпительного итога по дню -
-- нужно собрать транзакции по соотвесвтующему сроку
select * from sber_ml_replications.max_transaction;

-- таким образом мы имеем 2 сущности:
-- а) объект, который обозначит общее кол-во транзакций по всему периоду и клиента
-- c) расчет накопительного итога за день - по каждой карте
-- ниже результирующий запрос:
select vm.client_id,
       trx.trx_amt, trx.trx_dt,
       coalesce(trx.trx_amt + lag(trx.trx_amt) over ( -- coalesce для исключения  null в первых строчках группировки на атрибуте нарастающего итога:
           partition by trx.card_id order by trx.card_id, trx.trx_dt), trx.trx_amt) cumulative_total
from sber_ml_replications.trx_ref trx
inner join sber_ml_replications.max_transaction vm using (card_id)
where vm.cnt_transactions >= 18
order by trx.card_id, trx.trx_dt;

------------------------------------------------------------------------------------------------------------

-- в) В этом месяце запустили акцию на 10% кэшбека за каждую третью транзакцию.
-- Сколько уже выплатили кэшбека клиентам ?

-- для начала смотрим - что имеется в виду под "в этом месяце"
select trx_dt, count(trx_dt) from sber_ml_replications.trx_ref
group by trx_dt;

-- 2023-05-02,106
-- 2023-05-06,104
-- 2023-05-14,99
-- 2023-05-01,95
-- 2023-05-05,111
-- 2023-05-10,127
-- 2023-05-09,117
-- 2023-05-13,104
-- 2023-05-08,112
-- 2023-05-03,107
-- 2023-05-12,115
-- 2023-05-04,100
-- 2023-05-11,97

-- ок, весь период данных ограничен 1 месяцем, что сильно упрощают задачу
with cte_therd_trx as (
    select trx.card_id,
           trx.trx_amt,
           case
               when (row_number() over (order by card_id) % 3 = 0) then 1
               when (row_number() over (order by card_id) % 3 != 0) then 0 end cntr
    from sber_ml_replications.trx_ref trx
    where trx_amt != 0 and trx.card_id is not null
)
select distinct sum(case
           when cntr = 1 then (trx_amt / 100) * 10
           when cntr = 0 then cntr end) over ()
from cte_therd_trx;
------------------------------------------------------------------------------------------------------------
