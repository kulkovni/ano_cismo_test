/*
1) Мы хотим забирать данные себе, как можно оптимизировать изначальную структуру?
2) А какие поля стоит добавить с учетом особенностей обновления на источнике, чтобы не потерять данные?
 */

/* DDL */
-- start structure
drop table if exists sber_ml.clients;
drop table if exists sber_ml.cards;
drop table if exists sber_ml.trx;
create table sber_ml.clients (
    client_id serial primary key unique,
    gender varchar(1),
    age int4
);

create table sber_ml.cards(
    client_id bigint not null,
    card_id serial primary key unique,
    card_name varchar(50),
    open_dt date,
    close_dt date,
    constraint fc_clients foreign key (client_id)
                          references sber_ml.clients(client_id) on delete set null
);

create table sber_ml.trx (
    trx_id serial primary key,
    client_id bigint,
    card_id bigint,
    card_name varchar(50),
    trx_amt numeric(15),
    status varchar(6),
    trx_dt date,
    constraint fc_clients foreign key (client_id)
                          references sber_ml.clients(client_id) on delete set null,
    constraint fc_cards foreign key (card_id)
                         references sber_ml.cards(card_id) on delete no action
);

-- ######################################################################################################

-- DDL итог
drop table if exists sber_ml_replications.clients_ref;
create table sber_ml_replications.clients_ref as (
    select cli.client_id,
           case when cli.gender = 'F' then 1 when cli.gender = 'M' then 2 end gender_id,
           cli.age
    from sber_ml.clients cli);

drop table if exists sber_ml_replications.cards_ref;
create table sber_ml_replications.cards_ref as (
    with cte__ as (
select distinct c.client_id, c.card_id, c.card_name, c.open_dt, c.close_dt,
       max(trx.trx_dt) over (partition by trx.card_id) last_trx
from sber_ml.cards c
left join sber_ml.trx  trx using (card_id)
)
select c.client_id, c.card_id, c.card_name, c.open_dt,
        case
            when c.last_trx > c.close_dt then c.last_trx
            when c.last_trx <= c.close_dt then c.close_dt end closed_dt
from cte__ c);

drop table if exists sber_ml_replications.trx_ref;
create table sber_ml_replications.trx_ref as (
    select trx.card_id, trx_id,
           trx_amt, trx_dt,
           case when trx.status = 'SUCCES' then 1
               when trx.status = 'FAIL' then 2 end status_id
    from sber_ml.trx
);

-- также дать два справочника (что бы снизить накладные раскходы по физическому хранению
-- (строки весят больше интов))
drop table if exists sber_ml_replications.gender_list;
drop table if exists sber_ml_replications.trx_status_list;

create table sber_ml_replications.gender_list as (
    select distinct(gender), case
        when gender = 'F' then 1
        when gender = 'M' then 2 end id
    from sber_ml.clients
    where gender is not null
);

create table sber_ml_replications.trx_status_list as (
    select distinct(status), case
        when status = 'SUCCES' then 1
        when status = 'FAIL' then 2 end id
    from sber_ml.trx
    where status is not null
);

-- ######################################################################################################

/* накидываем ограничения и атрибуты */
alter table sber_ml_replications.gender_list add primary key (id);
alter table sber_ml_replications.trx_status_list add primary key (id);

-- в случае обновления нужно настроить тригерную функцию на UPD этого поля +
-- в условиях задачи не сказано, что на входе были предсотавлены исторические данные, и клиенты могут уже
-- потярять свой статус клиента
alter table sber_ml_replications.clients_ref
    add primary key (client_id);
alter table sber_ml_replications.clients_ref
    add column client_valid bool default true;
alter table sber_ml_replications.clients_ref
    add foreign key (gender_id)
    references sber_ml_replications.gender_list(id) on delete set null;
-- #####
alter table sber_ml_replications.cards_ref add primary key (card_id);
alter table sber_ml_replications.cards_ref add foreign key (client_id)
    references sber_ml_replications.clients_ref (client_id);
-- #####
alter table sber_ml_replications.trx_ref add primary key (trx_id);
alter table sber_ml_replications.trx_ref add foreign key (card_id)
    references sber_ml_replications.cards_ref (card_id);
alter table sber_ml_replications.trx_ref add foreign key (status_id)
    references sber_ml_replications.trx_status_list (id);

-- ######################################################################################################

/* 5) Для новой модели нужно создать агрегатную витрину на месячной основе с набором фичей. Например: id клиента,
отчетный месяц, сумма транзакций за месяц всего, количество транзакций за месяц всего. */

-- используем вьюшку, созданную в DML блоке для подсчета транзакций стр. 79

drop table if exists sber_ml_agg.report_at_moth;
create table sber_ml_agg.report_at_moth as (
    with cte_ as (
    select distinct mt.client_id,
        substring(trx.trx_dt::varchar, '-(\d{2})-')::int2 reporting_month,
        sum(trx.trx_amt) over (partition by trx.card_id order by mt.client_id),
        sum(trx.trx_amt) over (partition by mt.client_id) all_sum,
        mt.cnt_transactions cnt_trx_of_cars_at_period
    from sber_ml_replications.trx_ref trx
    left join sber_ml_replications.max_transaction mt using (card_id)
    order by client_id
    )
    select distinct client_id, reporting_month, all_sum,
           sum(cnt_trx_of_cars_at_period) over (partition by client_id) cntr_trx_at_period
    from cte_
    order by client_id);