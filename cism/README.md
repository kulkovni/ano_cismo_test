# Тестовое задание<br>
## Описание задачи 

1. Скачать набор данных о воздушном движении
https://clickhouse.com/docs/ru/getting-started/example-datasets/opensky/
2. Разбить скачанный файл на блоки (chunks)
3. Написать даг в AirFlow* для загрузки данных в PostgreSQL*.
    - НЕ должны загружаться рейсы, 
    у которых не указан origin или destination, 
    - НЕ должны загружаться рейсы, у которых origin и destination совпадают
4. Сформировать SQL-запрос: вывести callsign, registration, количество рейсов за всё время для callsign = ETD18U 
с разбивкой по каждому из registration (бортовому номеру самолета)

#### <i> Что ожидается в качестве результата выполнения задания:</i>
(ссылки кликабельны - ведут на файл ответа)

1. [Код AirFlow-дага из п.2 тестового задания](https://gitlab.com/kulkovni/ano_cismo_test/-/blob/v2/dags/dag_1.py)

2. [Код SQL формирования таблицы в PostgreSQL из п.2 тестового задания](https://gitlab.com/kulkovni/ano_cismo_test/-/blob/v2/queries/ano_cismo.sql)

3. [Код SQL-запроса из п.3 тестового задания (представлен отдельно)](https://gitlab.com/kulkovni/ano_cismo_test/-/blob/v2/queries/ano_cismo_ex_4.sql)

UPD (2023-06-06):
Добавлен вариант реализации посредством библиотеки `dask`, что позволило уменьшить время работы с даными на уровне задач,
хотя увеличило накладные расходы (внушительно)

1. [Код AirFlow-дага из п.2 тестового задания (реализация `dask`)](https://gitlab.com/kulkovni/ano_cismo_test/-/blob/v2/dags/dag_2.py)

- Также был скорректирован airflow.cfg по [рекмоендациям в документации](https://airflow-apache.readthedocs.io/en/latest/executor/dask.html)

При использвоании мультипроцессинга (`dask`) задачи отрабатывают заметно быстрее (разбивка на чанки ~4 минуты вместо ~16-20 минут в части разбивки данных)
    

- Имеет место специфический сценарий запуска:

при запуске airflow webserver и scheduler - необдимо такдже запуска DaskScheduler и worker/s, которые будут аккумуляровать ресурсы (что то вроде локального кластера)

    $: DASK_HOST=127.0.0.1; DASK_PORT=8786 
    $: dask-scheduler --host $DASK_HOST --port $DASK_PORT

и в соседнем терминале:

    $: DASK_HOST=127.0.0.1; DASK_PORT=8786
    $: dask-worker $DASK_HOST:$DASK_PORT --nworkers <n> --resources <"name_res">

где 
- <n> кол-во воркеров, 
- <"name_res"> - имя абстракции для обозначения `dask` пробрасываемым ресусрам


\* для удобства выполнения можно использовать docker образы
<i>(использовано гостевая система (ВМ) в виду ее готовности на момент начала выполнения)</i>

## Подробнее о предлагаемом решении

Инфраструктура (на гостевой системе): 
 
    - OS: Ubuntu 20.04.6 LTS x86_64
    - CPU: Intel Xeon (Icelake) (6) @ 1.995GHz 
    - GPU: 00:01.0 Vendor 1234 Device 1111
    - Memory: **** / 11489MiB

при реализации с `dask`:

    CPU: Intel Xeon (Icelake) (10) @ 1.995GHz
    Memory: **** / 20015MiB (но для текущего процесса хватает 16, до сбоя (но клауд не дает установить ограничение в 16 ГБ))

Используемые инструменты для работы с данным:

    - PostgreSQL 12
    - Airflow v 2.6.1
    - python 3.8
    
##### <i>(используемые модули в [reqruetments.txt](https://gitlab.com/kulkovni/ano_cismo_test/-/blob/main/reqruetments.txt))</i>

#### Преднастройки Postgre (минимальны):
- создание пользователей (личный для доступа из IDE + airflow_db для метаданных)
- выдача прав для пользователей
- явная установка состояния URI для airflow_db (что бы смотрел в public) (оно установлено по умолчанию, хотя есть ремарка в документации о 15 версии)
Но без этой установки не отрабатывал: `airflow db reset`

после того как было прописано вручную - установил ссылку на схему - все отработывало штатно

#### Преднастройки Airflow:
- [база данных по умолчанию - установлена postgre](https://airflow.apache.org/docs/apache-airflow/stable/howto/set-up-database.html#setting-up-a-postgresql-database:~:text=lib%3A%24LD_LIBRARY_PATH-,Setting%20up%20a%20PostgreSQL%20Database,-You%20need%20to)
- изменено <b>dags_folder = ~/ano_cismo_test/dags</b> для более удобного редактирования кода и предоставления директории в рамках настоящего проекта

- изменено <b>job_heartbeat_sec = 4200 </b> для увеличения времени жизни scheduler в долгих задачах

- изменено <b>task_queued_timeout = 4200.0</b> - для увеличения времени жизни scheduler в долгих задачах

- [изменено <b>executor = LocalExecutor</b> по рекомендации из документации](https://airflow.apache.org/docs/apache-airflow/2.6.1/core-concepts/executor/index.html#:~:text=change%20this%20to-,LocalExecutor,-for%20small%2C%20single)

- изменено <b>scheduler_heartbeat_sec = 30</b> - для снижение наклданых расходов

- изменено <b>in_file_process_interval = 50</b> - для снижение наклданых расходов

FYI: имело место исключение "zombie", в частности после отвала гостевой системы яндекса, в частности - в особо долгих задачах

Схема ETL процесса (примитив):

![ETL_SCHEMA](/etl_schema.jpg)

### Этапы выполнения: 

граф выполнения отслеживается в скрипте ~/ano_cismo_test/dags/dag_1.py


    get_facture_task >> drop_exists_table_task >> check_chank_dir_task >> read_facture_task >> push_cycle_task


1) `get_facture_task` - загрузка фактуры (в целях экономии времени в коде имеется проверка на наличие директории. В реальных условиях - скачивается заново) `(п.1 задачи)`

2) `drop_exists_table_task` - создается БД, куда будут складироваться данные `(п. 3 задачи)`

3) `check_chank_dir_task` - проверки наличия таблицы + директории  с чанкам (для своевременного upd) 

3) `read_facture_task` - разбивка входящей фактуру, полученной на `get_facture_task` (`п. 2 задачи`)

    \+ на этом же этапе выполняются проверки по условиям  `(п. 3 задачи)`
4) `push_cycle_task` - запись в БД (каждый чанк также разбивается на части, для экономии памяти)


### Краткое описание модуля utils/:

Нативный функционал, который по назначению призван декомпозировать код, сделать его более "удобочитаемым" и исключить повторения (максимально нивилировать :))

<i>(Субъективно - излишне интегрировать написанные методы в основные коллекции Airflow => просто вынесены в качестве  "нативного модуля" а не в плагины Airflow.)</i> 

- [utils/airflow_util.py](https://gitlab.com/kulkovni/ano_cismo_test/-/blob/v2/utils/airflow_util.py) - для установки некоторых параметров DAG по дефолтному состоянию (которому посчитает оператор DAGs)
- [utils/data_util.py](https://gitlab.com/kulkovni/ano_cismo_test/-/blob/v2/utils/data_util.py) - декомпозирует код основной функции DAG работы с данными - на отдельные функции (распаковка файла и его разбивка на chank`s)
- [utils/conf_util.py](https://gitlab.com/kulkovni/ano_cismo_test/-/blob/v2/utils/conf_util.py) - функционал доступа к локальным переменным (модуль environs) для сохранения кред, и иных конфигов в одном месте и получения доступа к ним из скриптов (+ немножко к ИБ)
- [util/db_util.py](https://gitlab.com/kulkovni/ano_cismo_test/-/blob/v2/utils/db_util.py) - модуль общения с БД (получилось весьма избыточно, т.к. в airflow.providers есть PostgreOperator, но сам модуль оставил и через него выполнялись некоторые задачи.)
- [utils/tmpdir_util.py](https://gitlab.com/kulkovni/ano_cismo_test/-/blob/v2/utils/tmpdir_util.py) - для работы с временными директориям и файлами (в основном подчищает фактуру и чанки, что бы не загрезнять место на хосте)
