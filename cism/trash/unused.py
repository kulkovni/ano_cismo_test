import logging
import os
import sys
from pathlib import Path
from airflow.operators.bash import BashOperator

sys.path.insert(0, f"{Path.home()}/ano_cismo_test")
from utils.conf_util import get_conf_param
from utils.data_util import PROJECT_PATH, make_the_frame
from utils.db_util import My_db_methods
from utils.tmpdir_util import del_file, del_tmp_path, make_tmp_dir


def spliting_facture_and_push_to_db(full_path_raw: str = None,
                                    table_name: str = "ano_cismo_test_fly",
                                    schema: str = "public",
                                    if_exists: str = "owerwrite") -> None:
    """ Будет разбивать данные сырой фактуры в цикле +
        выполнять необходимые фильтрации над сущностями данных и 
        записывать их в Postgre со своим суффиксом для дальнейшей операции исключения дубликатов

        :param full_path_raw: путь до сырой фактуры
        :param table_name: имя таблицы для записи в БД, defaults to "ano_cismo_test_fly"
        :param schema: схема БД для записи, defaults to "public"
        :param if_exists: что делать с данным, если таблица уже создана и имеет 
            некий состав даннных, defaults to "owerwrite"
    """
    # переменные полного пути:
    full_path_raw = f"{PROJECT_PATH}/facture" if full_path_raw is None else full_path_raw
    full_path_chank = f"{PROJECT_PATH}/chanked"
    
    files = [file for file in os.listdir(full_path_raw) if file != ".DS_Store"]
    
    for raw_file in files:
        split_one_file(raw_file)
        list_of_chanked_facture = [f"{full_path_chank}/{file}" for file in os.listdir(full_path_chank)
                        if file != MANIFEST_NAME]
        
        for file in list_of_chanked_facture:
            df = make_the_frame(file)
            logging.info(f"Фрейм сформирован из {file} - задача записи\n")
            My_db_methods().push_df_to_db(df=df, table_name=table_name,
                                          schema=schema,
                                          if_exists=if_exists)
            del df
            # gc.collect()
            del_file(path_to_file=file)
            
#####################################################################################################
# из утилит
def split_one_file(facture_file: str) -> None:
    """ Разобьет на части и запишет фактуру одного
        файла во временной директории
        
        :param facture_file: файл фактуры, который следует разбить
        return: None
    """
    # временная директория для хранения chank`s:
    path_to_csv = unziping_csv(facture_file)
    chank_of_one_file = make_tmp_dir(tmp_dir=f"{PROJECT_PATH}/chanked")
    
    raw_size = int(subprocess.check_output(f"wc -l {path_to_csv}",
                                           shell=True).decode("utf-8").split(" ")[0])
    # указание конфигурации для разбитых файлов:
    spliter = Split(path_to_csv, chank_of_one_file)
    spliter.manfilename = MANIFEST_NAME
    # разбивка:
    spliter.bylinecount(linecount=raw_size/25, includeheader=True)
    # зачистка от целого csv:
#    del_tmp_path()
    
#####################################################################################################

import gzip  

def unziping_csv(filename: str) -> str:
    """ Распаковка архива фактуры и сохранения в директории как единого файла
        (использовать по потребности и наличии места)
    :param filename: имя файла для распаковки
    :return: путь к распакованному файлу
    """
    full_path_to_filename = f"{PROJECT_PATH}/facture/{filename}"
    path_out = f"{make_tmp_dir()}/{str(filename[:-3])}"
    
    with gzip.open(full_path_to_filename, 'rt') as f:
        data = f.read()
        with open(path_out, 'wt') as f:
            f.write(data)
            # чистим память:
            del data
    return path_out

#####################################################################################################

import gc
import subprocess
from filesplit.split import Split

MANIFEST_NAME=get_conf_param("MANIFEST_NAME")


def split_one_file(facture_file: str) -> None:
    """ Разобьет на части и запишет фактуру одного
        файла во временной директории
        
        :param facture_file: файл фактуры, который следует разбить
        return: None
    """
    # временная директория для хранения chank`s:
    path_to_csv = unziping_csv(facture_file)
    chank_of_one_file = make_tmp_dir(tmp_dir=f"{PROJECT_PATH}/chanked")
    
    raw_size = int(subprocess.check_output(f"wc -l {path_to_csv}",
                                           shell=True).decode("utf-8").split(" ")[0])
    # указание конфигурации для разбитых файлов:
    spliter = Split(path_to_csv, chank_of_one_file)
    spliter.manfilename = MANIFEST_NAME
    # разбивка:
    spliter.bylinecount(linecount=raw_size/25, includeheader=True)
    # зачистка от целого csv:
    del_tmp_path()

#####################################################################################################

def sunday_zombies(**kwargs) -> None:
    """ Очистит шаг выполнения и перезапустит задачу по dag_id и task_id 
    в ту же дату, что и дата запуска DAG

    :param dag_id: id ДАГа
    :param task_id: id задачи
    """
    task_id = kwargs.get("task").task_id
    execution_date = kwargs.get("execution_date")
    
    sundays = BashOperator(
        task_id='sunday_zombies',
        bash_command=f'airflow tasks clear -s {execution_date}  -t {task_id} -d -y sunday_zombies'
    )
    return sundays.execute(context=kwargs)

#####################################################################################################

def decompose_facture_dir() -> dict:
    """ Разобъет список директорий и отдаст его в x_com

    :return: словарь директорий
    """
    facture_raw = f'{get_conf_param("PROJECT_PATH")}/facture'
    files = [f"{facture_raw}/{file}" for file in os.listdir(facture_raw)]
    
    result_di = dict(first_dirs=files[0:6],
                     second_dirs=files[6:12],
                     third_dirs=files[12:18],
                     fourth_dirs=files[18:24],
                     five_dirs=files[24:29],
                     six_dirs=files[29:]
                     )
    return result_di

#####################################################################################################

#    path_list = kwargs["ti"].xcom_pull(task_ids="decompose_facture_dir_task")[part]

#####################################################################################################

# # разбивка списка путей фактуры
    # decompose_facture_dir_task = PythonOperator(
    #     task_id="decompose_facture_dir_task",
    #     python_callable=decompose_facture_dir
    # ) 
    
    
    # chanking_task_second_task = PythonOperator(
    #     task_id="chanking_task_second_task",
    #     python_callable=chaked_frame,
    #     op_kwargs={"part": "second_dirs"},
    #     provide_context=True,
    # )
    
    # chanking_task_therd_task = PythonOperator(
    #     task_id="chanking_task_therd_task",
    #     python_callable=chaked_frame,
    #     op_kwargs={"part": "third_dirs"},
    #     provide_context=True,
    # )
    
    # chanking_task_fourth_task = PythonOperator(
    #     task_id="chanking_task_fourth_task",
    #     python_callable=chaked_frame,
    #     op_kwargs={"part": "fourth_dirs"},
    #     provide_context=True,
    # )
    
    # chanking_task_five_task = PythonOperator(
    #     task_id="chanking_task_five_task",
    #     python_callable=chaked_frame,
    #     op_kwargs={"part": "five_dirs"},
    #     provide_context=True,
    # )
    
    # chanking_task_six_task = PythonOperator(
    #     task_id="chanking_task_six_task",
    #     python_callable=chaked_frame,
    #     op_kwargs={"part": "six_dirs"},
    #     provide_context=True,
    # )
    
    
    
    # FROM DAG
    
            
# def push_tmp_to_db(table_name: str, 
#                schema: str = "public", 
#                if_exists: str = "overwrite",
#                **kwargs) -> None:
#     """ обойдет всю разбитую фактуру и запишет в БД

#     :param table_name: имя таблицы под запись
#     :param schema: схема таблицы под запись 
#     :param if_exists: что делать если есть таблица, defaults to "overwrite"
#     """
#     import gc
    
#     chank_dir_path = kwargs["ti"].xcom_pull(task_ids="check_chank_dir_task")
#     file_list = (f"{chank_dir_path}/{file}" for file in os.listdir(f"{chank_dir_path}"))
    
#     for file in file_list:
#         df = make_the_frame(file)
#         for_log = re.search(r'(flightlist.*\d)', file).group(0)
#         logging.info(f"{for_log} отправлен БД")
#         My_db_methods().push_df_to_db(df=df, table_name=table_name,
#                                         schema=schema, if_exists=if_exists)
        
#         del df 
#         del for_log
#         gc.collect()
        
#     del_tmp_path(CHANKED_PATH)


    # check_chank_dir_task = PythonOperator(
    #     task_id="check_chank_dir_task",
    #     python_callable=check_chank_dir
    # )
    
    # # отправка в БД
    # push_to_db_task = PythonOperator(
    #     task_id="push_to_db_task",
    #     python_callable=push_tmp_to_db,
    #     op_kwargs={
    #         "table_name": "ano_cismo_opensky",
    #         "if_exists": "append"
    #         },
    #     provide_context=True
    # )
    
    #  # зачистить x_com:
    # @task
    # @provide_session
    # def xcom_del(task_id: str, dag_id: str, session=None) -> None:
    #     session.query(XCom).filter(XCom.task_id == task_id, XCom.dag_id == dag_id).delete()
    #     session.commit()
    #     logging.info(f"xcom=task_id: {task_id} успешно удален...")
    
    # get_facture_task >> \
    #     [check_chank_dir_task, clearing_result_table_if_exists_task] >> \
    #             chanking_task >> \
    #                 push_to_db_task >> \
    #                         xcom_del(task_id="check_chank_dir_task",
    #                                 dag_id=DAG_ARGUMENTS["dag_id"])
    