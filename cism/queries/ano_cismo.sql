drop table if exists ano_cismo_opensky;
create table ano_cismo_opensky (
    callsign varchar(64) ,
    number varchar(64),
    icao24 varchar(64),
    registration varchar(64),
    typecode varchar(64),
    origin varchar(64),
    destination varchar(64),
    firstseen timestamptz,
    lastseen timestamptz,
    "day" timestamptz,
    latitude_1 double precision,
    longitude_1 double precision,
    altitude_1 float4,
    latitude_2 double precision,
    longitude_2 double precision ,
    altitude_2 float4);
