import os
import sys
import logging
from pathlib import Path
from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.postgres.operators.postgres import PostgresOperator

sys.path.insert(0, f"{os.path.expanduser('~/')}ano_cismo_test")
from utils import QUERY_CRE
from utils.tmpdir_util import make_tmp_dir
from utils.conf_util import get_conf_param
from utils.airflow_util import create_default_args
from utils.data_util import FACTURE_PATH, CHANKED_PATH, get_facture, chanking_facture_dask, pusher_dask


# Константы дага:
OWNER = get_conf_param("AIRFLOW_OWNER")
DEFAULT_ARGS = create_default_args(owner=OWNER, retries=3, start_date=datetime.now())
FACTURE_IS_LOADED = False if os.path.exists(f'{get_conf_param("PROJECT_PATH")}/facture') else True

LIST_OF_CHUNKS = [f"{CHANKED_PATH}/{name}" for name in os.listdir(path=f'{CHANKED_PATH}')]


DAG_ARGUMENTS = {
    "dag_id": f"{Path(__file__).stem}",
    "tags": ["ano_cismo_test"],
    "default_args": DEFAULT_ARGS,
    "dagrun_timeout": timedelta(minutes=720),
    "schedule_interval": "@once",
    "catchup": False,
}

def check_chank_dir(path_to_facture: str = CHANKED_PATH) -> None:
    """Проверка / создание папки для чанков """
    path_to_facture = make_tmp_dir(path_to_facture)
    logging.info(f"Директория под разбитую фактуру находится по пути: {path_to_facture}")


with DAG (**DAG_ARGUMENTS) as dag:
    
    get_facture_task = PythonOperator(
        task_id="get_facture_task",
        python_callable=get_facture,
        op_kwargs={"rewrite": FACTURE_IS_LOADED }
    )
    
    drop_exists_table_task = PostgresOperator(
        task_id="drop_exists_table_task",
        postgres_conn_id="ano_cismo",
        autocommit=True,
        sql=QUERY_CRE,
    )
    
    check_chank_dir_task = PythonOperator(
        task_id="check_chank_dir_task",
        python_callable=check_chank_dir
    )
    
    chanking_facture = PythonOperator(
        task_id="read_facture_task",
        python_callable=chanking_facture_dask,
        op_kwargs = {"facture_path": FACTURE_PATH}
    )
    
    pusher_task = PythonOperator(
        task_id="push_cycle_task",
        python_callable=pusher_dask,
        op_kwargs={"list_of_chunks": LIST_OF_CHUNKS,
                   "list_of_columns": ["callsign",
                                        "number",
                                        "icao24",
                                        "registration",
                                        "typecode",
                                        "origin",
                                        "destination",
                                        "firstseen",
                                        "lastseen",
                                        "day",
                                        "latitude_1",
                                        "longitude_1",
                                        "altitude_1",
                                        "latitude_2",
                                        "longitude_2",
                                        "altitude_2"],
                       "if_exists": "append",
                       "chunksize": 100,
                       "dask_scheduler": "processes"}
    )
    
    get_facture_task >> drop_exists_table_task >> check_chank_dir_task >> chanking_facture >> pusher_task
