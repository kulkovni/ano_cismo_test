import re
import os
import sys
import logging
import time
from pathlib import Path
from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.postgres.operators.postgres import PostgresOperator

sys.path.insert(0, f"{os.path.expanduser('~/')}ano_cismo_test")
from utils import QUERY_CRE
from utils.tmpdir_util import make_tmp_dir
from utils.conf_util import get_conf_param
from utils.airflow_util import create_default_args
from utils.data_util import FACTURE_PATH, CHANKED_PATH, get_facture, chanking_facture, pusher_pd_pq


# Константы дага:
OWNER = get_conf_param("AIRFLOW_OWNER")
DEFAULT_ARGS = create_default_args(owner=OWNER, retries=3, start_date=datetime.now())
FACTURE_IS_LOADED = False if os.path.exists(f'{get_conf_param("PROJECT_PATH")}/facture') else True


DAG_ARGUMENTS = {
    "dag_id": f"{Path(__file__).stem}",
    "tags": ["ano_cismo_test"],
    "default_args": DEFAULT_ARGS,
    "dagrun_timeout": timedelta(minutes=60),
    "schedule_interval": "@once",
    "catchup": False,
    "dagrun_timeout": timedelta(hours=16),
}

def check_chank_dir(path_to_facture: str = CHANKED_PATH) -> None:
    """Проверка / создание папки для чанков """
    path_to_facture = make_tmp_dir(path_to_facture)
    logging.info(f"Директория под разбитую фактуру находится по пути: {path_to_facture}")


def read_facture(path_to_facture: str = FACTURE_PATH) -> None:
    """ наполнит директорию разбитыми данными из файлов фактуры
    
    :param part: часть списка директорий под разделение
    """
    path_list_gen = (f"{path_to_facture}/{file}"for file in os.listdir(path_to_facture))
     
    for file in path_list_gen:
        tic = time.perf_counter()
        chanking_facture(facture_path=file)
        tak = time.perf_counter()
        for_log = re.search(r'(flightlist.*\d)', file).group(0)
        logging.info(f"{for_log} РАЗБИТ Разбивка заняла {tak - tic:0.4f} секунд")


def push_cycle(**kwargs) -> None:
    """ прочтет все чанки - и запишет их в БД

    :param facture_paths: список путей для чтения
    """

    facture_paths = (f"{CHANKED_PATH}/{file}"for file in os.listdir(CHANKED_PATH))
    
    pusher_pd_pq(facture_paths,
                schema=kwargs.get("schema"),
                if_exists=kwargs.get("if_exists"),
                table_name=kwargs.get("table_name"))
        

with DAG (**DAG_ARGUMENTS) as dag:
    
    get_facture_task = PythonOperator(
        task_id="get_facture_task",
        python_callable=get_facture,
        op_kwargs={"rewrite": FACTURE_IS_LOADED }
    )
    
    drop_exists_table_task = PostgresOperator(
        task_id="drop_exists_table_task",
        postgres_conn_id="ano_cismo",
        autocommit=True,
        sql=QUERY_CRE,
    )
    
    check_chank_dir_task = PythonOperator(
        task_id="check_chank_dir_task",
        python_callable=check_chank_dir
    )
    
    read_facture_task = PythonOperator(
        task_id="read_facture_task",
        python_callable=read_facture
    )
    
    push_cycle_task = PythonOperator(
        task_id="push_cycle_task",
        python_callable=push_cycle,
        op_kwargs ={"schema": "public",
                    "table_name": "ano_cismo_opensky",
                    "if_exists": "append",
                    }
    )
    
    get_facture_task >> drop_exists_table_task >> check_chank_dir_task >> read_facture_task >> push_cycle_task
