import environs


env = environs.Env()
env.read_env()


def get_db_conf() -> dict:
     """ Вернут объект конфига для базы, с которой ведется работа

     :return: словарь конфига для движка
     """
     return dict(host=env("HOST_DB"), port=env("PORT_DB"),
                 database=env("DB_NAME"), 
                 user=env("USER_DB"),
                 password=env("PASSWORD_DB"))
     
 
def get_conf_param(param: str) -> str:
     """ return confedentional cred or parametr

     :param param: string name ofr parametr
     :return: string of value
     """
     return env(param)


if __name__ == "__main__":
    print(get_db_conf('dwh'))
    