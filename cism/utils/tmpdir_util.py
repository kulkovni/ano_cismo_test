import os 
import sys

sys.path.insert(0, f"{os.path.expanduser('~/')}ano_cismo_test")
from utils.conf_util import get_conf_param

tmp_dir = f"{get_conf_param('PROJECT_PATH')}/csv_facture_tmp"


def del_tmp_path(tmp_dir: str = tmp_dir) -> None:
    """Удалить временную директорию

    :param tmp_dir: путь для удаления, defaults to tmp_dir
    """
    os.system(f"rm -r {tmp_dir}")


def make_tmp_dir(tmp_dir: str = tmp_dir) -> str:
    """ Создать временную директорию

    если директория задана - перепишет
    :param tmp_dir: путь, defaults to tmp_dir
    :return: созданный путь
    """
    if not os.path.exists(tmp_dir):
        os.system(f"mkdir {tmp_dir}")
    else:
        del_tmp_path(tmp_dir)
        make_tmp_dir(tmp_dir)
    return tmp_dir

def del_file(path_to_file: str) -> None:
    os.system(f"rm {path_to_file}")


if __name__ == "__main__":
    make_tmp_dir("/home/kulkovni/ANO_CISMO_TEST/facture")