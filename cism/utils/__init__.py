from polars import Float64, Datetime, Utf8

QUERY_CRE = """ drop table if exists ano_cismo_opensky;
                create table ano_cismo_opensky (
                    callsign varchar(64) ,
                    number varchar(64),
                    icao24 varchar(64),
                    registration varchar(64),
                    typecode varchar(64),
                    origin varchar(64),
                    destination varchar(64),
                    firstseen timestamptz,
                    lastseen timestamptz,
                    "day" timestamptz,
                    latitude_1 double precision,
                    longitude_1 double precision,
                    altitude_1 float4,
                    latitude_2 double precision,
                    longitude_2 double precision ,
                    altitude_2 float4
                );
"""

DTYPES_FOR = {"callsign":"str",
            "number":"str",
            "icao24":"str",
            "registration":"str",
            "typecode":"str",
            "origin":"str",
            "destination":"str",
            "latitude_1":"float64",
            "longitude_1":"float64",
            "altitude_1":"float64",
            "latitude_2":"float64",
            "longitude_2":"float64",
            "altitude_2":"float64"}


DTYPES_FOR_POLARS = {
    "callsign": Utf8,
    "number": Utf8,
    "icao24": Utf8,
    "registration": Utf8,
    "typecode": Utf8,
    "origin": Utf8,
    "destination": Utf8,
    "firstseen": Datetime,
    "lastseen": Datetime,
    "day": Datetime,
    "latitude_1": Float64,
    "longitude_1": Float64,
    "altitude_1": Float64,
    "latitude_2": Float64,
    "longitude_2": Float64,
    "altitude_2": Float64
    }
