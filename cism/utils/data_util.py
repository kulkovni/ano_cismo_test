import os
import re
import gc
import sys
import time
import logging
import pandas as pd
import polars as pl
from dask import dataframe as dd
from datetime import datetime
from pyarrow import parquet as pq

from airflow.exceptions import AirflowException

sys.path.insert(0, f"{os.path.expanduser('~/')}Desktop/etl_work/ano_cismo_test")

from utils import DTYPES_FOR, DTYPES_FOR_POLARS
from utils.tmpdir_util import make_tmp_dir
from utils.conf_util import get_conf_param
from utils.db_util import My_db_methods


# Константа где хранится фактура:
PROJECT_PATH = get_conf_param("PROJECT_PATH")
FACTURE_PATH = f"{PROJECT_PATH}/facture"
CHANKED_PATH = f"{PROJECT_PATH}/chanked"


def get_facture(storage_path: str = None, **kwargs) -> None:
    """ заберет данные из первоисточника и сложит в указанный storage_path
    в случае наличия данных по пути - перезапишет весь storage_path

    :param storage_path: путь загрузки, _default_ None
    :param: kwargs['rewrite'] - в качесвте флага, который определяет пользователь
        если kwargs['rewrite'] == True - задача перезапишет текущую папку фактуры по определнному пути
        если kwargs['rewrite'] ==  False - задача не будет давать запрос на загрзуку фактуры
        но - рекомендовано явно указывать директорию в рамках описания задачи (исключение может быть 
        только в том случае если 100% уверены в наличии фактуры по дефолтному состония)
    """
    try:
        default_path = f"{get_conf_param('PROJECT_PATH')}/facture"
        if kwargs.get("rewrite", True):
            if storage_path is None :
                storage_path = make_tmp_dir(tmp_dir=default_path)
            else:
                storage_path = make_tmp_dir(tmp_dir=storage_path)
            
            logging.info(f"Загрузка в storage по пути: {storage_path}")
            add_target_storage = f"-P {storage_path}"
            wget_cmd = f"wget -O- https://zenodo.org/record/5092942 | grep -oP 'https://zenodo.org/record/5092942/files/flightlist_\d+_\d+\.csv\.gz' | xargs wget {add_target_storage}"
            os.system(wget_cmd)
        else:
            storage_path = storage_path if storage_path is not None else default_path
            logging.info(f"Загрузка в storage не требуется по заявлению пользователя. Беру фактуру из {storage_path}")
    except Exception as e:
        logging.error(f"Во время загрузки данных произошло исключение: {e}")
        raise AirflowException


def chanking_facture(facture_path: str) -> None:
    """ Функция для разбивки данных и записи в данных в БД

    :param path_to_file: _description_
    :param flag_file: _description_, defaults to True
    """
    chank_name = re.search(r"(flightlist.*\d)",facture_path).group(0)
    
    for chunk_df in pd.read_csv(facture_path, 
                                header=0, 
                                sep=",",
                                compression="gzip",
                                chunksize=200000,
                                parse_dates=["firstseen", "lastseen", "day"]):
        
        date_sfx = str(datetime.now().microsecond)
        
        save_path = f'{CHANKED_PATH}/{chank_name}_{date_sfx}.parquet'                    
        chunk_df.to_parquet(path=save_path,
                            engine='pyarrow', 
                            compression='snappy',
                            index=False)
        

def pusher_pd_pq(facture_paths: list, **kwargs) -> None:
    """запись чанка в БД

    :param facture_paths: _description_
    """
    
    for file in facture_paths:
        tic = time.perf_counter()
        df = pd.read_parquet(file, engine="pyarrow")
        # обеспечиваем условия фильтрации:
        df = df[ df["destination"] != df["origin"] ]
        df = df[( df["destination"].notna() ) | ( df["origin"].notna() )]
        
        df = df.astype(kwargs.get("retyper", DTYPES_FOR))
        
        My_db_methods().push_df_to_db(df=df, 
                                    table_name=kwargs.get("table_name", "ano_cismo_opensky"),
                                    schema=kwargs.get("schema", "public"),
                                    if_exists=kwargs.get("if_exists", "append"),
                                    chunks=50000)
        del df
        gc.collect()
        
        tak = time.perf_counter()
        chank_name = re.search(r'(flightlist.*\d)', file).group(0)
        logging.info(f"Запись чанка из файла {chank_name} за {tak - tic:0.4f} секунд")
        
        
def pusher_pd_pq(facture_paths: list, **kwargs) -> None:
    """запись чанка в БД

    :param facture_paths: _description_
    """
    
    for file in facture_paths:
        tic = time.perf_counter()
        df = pd.read_parquet(file, engine="pyarrow")
        # обеспечиваем условия фильтрации:
        df = df[ df["destination"] != df["origin"] ]
        df = df[( df["destination"].notna() ) | ( df["origin"].notna() )]
        
        df = df.astype(kwargs.get("retyper", DTYPES_FOR))
        
        My_db_methods().push_df_to_db(df=df, 
                                    table_name=kwargs.get("table_name", "ano_cismo_opensky"),
                                    schema=kwargs.get("schema", "public"),
                                    if_exists=kwargs.get("if_exists", "append"),
                                    chunks=50000)
        del df
        gc.collect()
        
        tak = time.perf_counter()
        chank_name = re.search(r'(flightlist.*\d)', file).group(0)
        logging.info(f"Запись чанка из файла {chank_name} за {tak - tic:0.4f} секунд")
  

# вариант реализации через dask.dataframe  
def chanking_facture_dask(facture_path: str) -> None:
    """ Функция для разбивки данных и записи в данных в БД
        (используется dask.dataframe.
        
        Профит: сокращение времени обработки с 16 минут до 4 минут
        
    :param path_to_file: _description_
    :param flag_file: _description_, defaults to True
    """
    
    df = dd.read_csv(f"{facture_path}/flightlist_*_*.csv.gz", 
                parse_dates=["firstseen", "lastseen", "day"],
                compression="gzip",
                blocksize="64MB",
                header=0, 
                sep=",",
                dtype=DTYPES_FOR)

    df = df[ (df["destination"].notnull()) | df["origin"].notnull()]
    df = df[df["destination"]!=df["origin"]]

    logging.info("Запуск процесса дролбления данных ...")
    tic = time.perf_counter()
    dd.to_parquet(df=df, path=CHANKED_PATH, 
                engine="pyarrow", compression="snappy", 
                write_index=False,
                compute=True, compute_kwargs={"scheduler": "processes"})
    tac = time.perf_counter()
    logging.info(f"Разбивка данных завершена. Данные сохранены по пути: {facture_path}  Затраченное время: {tac - tic:0.4f} секунд")
    

def pusher_dask(list_of_columns: list, list_of_chunks: list, **kwargs) -> None:
    """ запись чанка в БД

    :param list_of_columns: _description_
    :param list_of_chunks: _description_
    """
    
    uri = get_conf_param("URI")
    logging.info("Запуск процесса записи в БД")
    
    
    for chunk in list_of_chunks:
        
        pq_df = dd.read_parquet(path=list_of_chunks, 
                            engine="pyarrow", 
                            columns=list_of_columns, 
                            dtype_backend="pyarrow", 
                            parquet_file_extension=".parquet")
        
        pusher = dd.to_sql(df=pq_df, 
                        name="ano_cismo_opensky", 
                        schema="public",
                        index=False,
                        method= "multi",
                        if_exists=kwargs.get("if_exists","append"), 
                        chunksize= kwargs.get("chunksize", 10000),
                        uri=kwargs.get("", uri) ,
                        compute=False)
            
        logging.info(f"Запись части {chunk} в БД")
        tic = time.perf_counter()
        pusher.compute(scheduler=kwargs.get("dask_scheduler", None))
        tac = time.perf_counter()
        logging.info(f"Запись {chunk} в базу данных завершена. Затраченное время: {tac - tic:0.4f} секунд")
        
        del pq_df
        del pusher
        
        gc.collect()\
            

# вариант реализации на PySpark:
def spark_chanker() -> None:
    pass

def pusher_to_db_at_spark() -> None:
    pass
    
    
# вариант реализации на Polars
def polars_chanker(file: str) -> None:
    # for file in path_list
    df = pl.read_csv_batched(source=file,
                            separator=",",
                            columns = ["callsign","number","firstseen","lastseen", "day"],
                            has_header=True,
                            infer_schema_length=100,
                            low_memory=False,
                            dtypes=DTYPES_FOR_POLARS,
                            try_parse_dates=True,
                            n_threads=7,
                            batch_size=1000,
                            sample_size=100,
                            encoding="binary")
    print(type(df))
    
    print(df.next_batches(5))
    
    # path = f"{os.path.expanduser('~/')}Desktop/etl_work/ano_cismo_test/chanked/test.parquet"
    
    # df.write_parquet(file=path, compression="snappy",
    #                  use_pyarrow=True,
    #                  row_group_size = 1000)
    
    # df = pl.read_csv(source=file, 
    #                  separator=",",
    #                  has_header=True, 
    #                  infer_schema_length=100,
    #                  low_memory=False,
    #                  dtypes=DTYPES_FOR_POLARS,
    #                  try_parse_dates=True,
    #                  n_threads=7,
    #                  batch_size=1000) \
    #                      .select(pl.col(["callsign","number","firstseen","lastseen", "day"]))
    
    
    # df = pl.scan_csv(source=file, separator=",",
    #                 has_header=True, 
    #                 cache=False, null_values=["NaN", "nan", "null", "Null", "None", "none", None],
    #                 infer_schema_length=100,
    #                 low_memory=False,
    #                 dtypes=DTYPES_FOR_POLARS,
    #                 try_parse_dates=True)
    # 


def polars_pusher_to_db() -> None:
    pass


if __name__ == "__main__":
    
    path = f"{os.path.expanduser('~/')}Desktop/etl_work/ano_cismo_test/facture/flightlist_20190101_20190131.csv.gz"
    
    df = polars_chanker(file=path)