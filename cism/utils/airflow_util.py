from datetime import timedelta
from airflow.utils.dates import days_ago

def create_default_args(owner, **kwargs) -> dict:
    """Вспомогательная функция для генерации аргументов DAG по умолчанию

    :param owner: УЗ пользователя, под которым будет запущена задача
    :return: словарь для работы с DAG
    """
    default_args = {
        "owner": owner,
        "start_date": kwargs.get("start_date", days_ago(0)),
        "execution_timeout": kwargs.get("execution_timeout", timedelta(minutes=700)),
        "run_as_owner": kwargs.get("run_as_owner", True)
    }
    
    return {**default_args, **kwargs}
