import sys
import os
import pandas as pd
import psycopg2 as ps2
import sqlalchemy
from pandas import DataFrame

sys.path.insert(0, f"{os.path.expanduser('~/')}ano_cismo_test")
from utils.conf_util import get_db_conf


class My_db_methods:
    """ Класс для работы с базой данных в рамках поставленной задачи проекта """
    def __init__(self) -> None:

        self.conf_db = get_db_conf()
        self.db_alias = "postgresql"
    
    def get_cursor_and_conn_db(self) -> dict:
        """
        Вернет словрь, содержащий объект-курсор БД и конфиг

        :return: словарь(курсор=курсор, конфиг=конфиг)
        """
        conn = ps2.connect(
            host=self.conf_db["host"],
            port=self.conf_db["port"],
            database=self.conf_db["database"],
            user=self.conf_db["user"],
            password=self.conf_db["password"]
            )
        
        cursor = conn.cursor()
        return dict(cursor=cursor, conn=conn)

                
    def _get_db_engine(self) -> callable:
        """
        Инициализирует engine для работы с БД
        """
        user, pwd = self.conf_db['user'], self.conf_db['password']
        host, port, db = self.conf_db['host'], self.conf_db['port'], self.conf_db['database']
        
        conf_str = f"{self.db_alias}://{user}:{pwd}@{host}:{port}/{db}"
        engine = sqlalchemy.create_engine(conf_str) 
        return engine

    def push_df_to_db(self, df: DataFrame,
                            table_name: str, 
                            schema: str, 
                            if_exists: str = "replace",
                            dtype_df: dict = None,
                            chunks: int = None) -> None:
        """
        Запишет в postgre pd.DataFrame, передынный в df

        :param df: DataFrame под запись
        :param table_name: имя новой таблицы в БД
        """
        con = self._get_db_engine()
        
        if dtype_df is not None:
            df = df.astype(dtype_df)
        
        df.to_sql(name=table_name, 
                  schema=schema, 
                  con=con,
                  if_exists=if_exists,
                  index=False,
                  method="multi", 
                  chunksize=chunks)
    
    def get_allData_fromDB_to_pd(self, 
                                schema: str,
                                table_name: str) -> DataFrame:
        """ Простой оператор select всех данных, для быстрого доступа
        к определнным в аргументах схеме и таблице

        :param schema: schema в БД
        :param table_name: таблица в БД
        :return: pd.DataFrame считанный из БД
        """
        
        conn = self.get_cursor_and_conn_db()["conn"]
        df = pd.read_sql_query(sql=f"SELECT * FROM {schema}.{table_name}", 
                               con=conn)
        conn.close()
        return df
    
    def push_query_to_db(self, query: str) -> None:
        """ Метод для отправки произвольных запросов в БД
        (в рамках работы с AirFlow - избыточен)
        :param query: тело запроса
        """
        
        cc = self.get_cursor_and_conn_db()
        
        cc["cursor"].execute(query)
        cc["conn"].commit()
        cc["conn"].close()


if __name__ == "__main__":
    test_obj = My_db_methods("lake")
    
    test_obj.push_query_to_db(query="CREATE TABLE test.test_table (id VARCHAR (3));")
